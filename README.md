# go-james-with-gitlab-ci

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/apavanello/go-james-with-gitlab-ci)](https://goreportcard.com/report/gitlab.com/apavanello/go-james-with-gitlab-ci) [![Documentation](https://godoc.org/gitlab.com/apavanello/go-james-with-gitlab-ci?status.svg)](http://godoc.org/gitlab.com/apavanello/go-james-with-gitlab-ci)

Gitlab-CI test example 

Command:
``` bash
.\go-james.exe new --path $GOPATH\src\gitlab.com\apavanello\go-james-with-gitlab-ci --package gitlab.com/apavanello/go-james-with-gitlab-ci --with-gitlab-ci --with-git
```
